import * as general from "@/translations/es/general";
import {buttons} from "@/translations/en/buttons";
export default {
    es: {
        ...general,
        ...buttons
    }
}
