export const buttons = {
    ok: "Ok",
    cancel: "Cancel",
    edit: "Edit",
    submit: "Submit",
    close: "Close",
}
