import BaseEntityService from './baseEntityService';
import type { IUser } from '@/shared/models/UserModel'
export default class  UserService extends BaseEntityService<IUser> {
    private static instance: UserService;

    private constructor() {
        super('/api/users/me')
    }

    public static getInstance(): UserService {
        if (!UserService.instance) {
            return new UserService()
        }
        return UserService.instance
    }
}
