import type {Moment} from "moment";

export interface IBaseModel {
    id?: Number;
    createdOn?: Moment;
    updatedOn?: Moment;
}

export default class BaseModel implements IBaseModel {
    constructor(
        public id?: Number,
        public createdOn?: Moment,
        public updatedOn?: Moment,
    ){
    }
};
