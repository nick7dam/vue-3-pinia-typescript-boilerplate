import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createI18n } from 'vue-i18n'
import App from './App.vue'
import router from './router'
import '@/assets/base.scss'
import en from '@/translations/en'
import es from '@/translations/es'
const app = createApp(App)
const messages = {
    ...en,
    ...es
}
const i18n = createI18n({
    locale: 'en',
    fallbackLocale: 'en',
    messages,
})
app.use(createPinia())
app.use(router)
app.use(i18n)

app.mount('#app')
